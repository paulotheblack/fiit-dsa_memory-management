/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 4:
 *      1. Zaplnenie celej pamate po blokoch velkosti 28B (+4B HEADER)
 *      2. Uvolnenie bloku na offsete 100
 *          First_Free_Offset == 100
 *          First_Free_Size == 32
 *      3. Uvolnenie bloku na offsete 132 (Merge zprava)
 *          First_Free_Offset == 100
 *          First_Free_Size == 64
 *      4. Uvolnenie bloku na offsete 68 (Merge zlava)
 *          First_Free_Offset == 68
 *          First_Free_Size == 96
 *
 * Expected results:
 *      Malloc Requests: 8
 *      Malloc Correct: 8
 *      Ratio of success 100.00%
 *
 *      Free Requests: 3
 *      Free Correct: 3
 *      Success ratio 100.00%
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 260

int main() {
    char region[TEST_SIZE];

    float malloc_requests = 0;
    float malloc_correct = 0;
    float malloc_ratio = 0;

    float free_requests = 0;
    float free_correct = 0;
    float free_ratio = 0;

    char *a = NULL;
    char *b = NULL;
    char *c = NULL;

    memory_init(region, TEST_SIZE);

    for (int i = 0; i < 2; i++) {
        malloc_requests += 1;
        if (memory_alloc(28) != NULL) malloc_correct += 1;
    }

    malloc_requests += 1;
    if ((a = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((b = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((c = memory_alloc(28)) != NULL) malloc_correct += 1;

    for (int i = 0; i < 3; i++) {
        malloc_requests += 1;
        if (memory_alloc(28) != NULL) malloc_correct += 1;
    }
    debug_print(region, TEST_SIZE);

    if (b != NULL) {
        free_requests += 1;
        if (memory_free(b) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);

    if (c != NULL) {
        free_requests += 1;
        if (memory_free(c) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);

    if (a != NULL) {
        free_requests += 1;
        if (memory_free(a) == 0) free_correct += 1;
    }
    debug_print(region, TEST_SIZE);

    malloc_ratio = (malloc_correct / malloc_requests) * 100;
    free_ratio = (free_correct / free_requests) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tMalloc Requests: %.0f\n", malloc_requests);
    printf("\t\tMalloc Correct: %.0f\n", malloc_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", malloc_ratio);

    printf("\n\t\tFree Requests: %.0f\n", free_requests);
    printf("\t\tFree Correct: %.0f\n", free_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", free_ratio);

    return 0;
}