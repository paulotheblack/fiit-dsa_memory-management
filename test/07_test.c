/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 7: Hanging bytes at the end of memory
 *      Alokujeme cely blok pamate, no na konci nechame volnych =< HEADER_SIZE B,
 *      ktore ostanu nealokovane. Budeme testovat ci pri naslednom uvoleneni
 *      najblizsie bloku dojde k spojeniu tychto blokov
 *
 * Expected results:
 *      Free Requests: 2
 *      Free Merge: 2
 *      Success ratio: 100.00%
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 132

int main() {
    char region[TEST_SIZE];

    float free_requests = 0;
    float free_correct = 0;
    float free_ratio = 0;

    char *a = NULL;


    memory_init(region, TEST_SIZE);

    for (int i = 0; i < 7; i++) {
        memory_alloc(12);
    }

    a = memory_alloc(9);
    debug_print(region, TEST_SIZE);

    struct header *last_header = get_header(116);

    if (a != NULL) {
        free_requests += 1;
        memory_free(a);
    }

    if (last_header->size == 16) free_correct += 1;
    debug_print(region, TEST_SIZE);

    a = memory_alloc(7);
    debug_print(region, TEST_SIZE);

    if (a != NULL) {
        free_requests += 1;
        memory_free(a);
    }

    if (last_header->size == 16) free_correct += 1;
    debug_print(region, TEST_SIZE);

    free_ratio = (free_correct / free_requests) * 100;

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");
    printf("\t\tFree Requests: %.0f\n", free_requests);
    printf("\t\tFree Merge: %.0f\n", free_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", free_ratio);

    return 0;
}

