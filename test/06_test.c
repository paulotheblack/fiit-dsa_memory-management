/*
 * @author: Michal Paulovic (xpaulovicm1)
 *
 * @scenario 6:
 *      Zaplnenie celej pamate a uvolnenie kazdeho 2. bloku v poradi.
 *      Kontrola ci je explicitny zoznam volnych blokov spravny.
 *      Bloky su alokovane v porade: X, a, X, b, X, c, X, d
 *      Bloky su uvolnovane v poradi: d, b, c, a
 *
 * Expected results:
 *      Overview List of free blocks
 *      Current Offset: 0
 *      Next Free at Offset: 36
 *      Next Free at Offset: 100
 *      Next Free at Offset: 164
 *      Next Free at Offset: 228
 *      Current Offset: 228 (Last free)
 * */

#include <stdio.h>
#include "../src/memalloc.h"

#define TEST_SIZE 260

int main() {
    char region[TEST_SIZE];

    float malloc_requests = 0;
    float malloc_correct = 0;
    float malloc_ratio = 0;

    float free_requests = 0;
    float free_correct = 0;
    float free_ratio = 0;

    char *a = NULL;
    char *b = NULL;
    char *c = NULL;
    char *d = NULL;

    memory_init(region, TEST_SIZE);

    malloc_requests += 1;
    if (memory_alloc(28) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((a = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if (memory_alloc(28) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((b = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if (memory_alloc(28) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((c = memory_alloc(28)) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if (memory_alloc(28) != NULL) malloc_correct += 1;

    malloc_requests += 1;
    if ((d = memory_alloc(28)) != NULL) malloc_correct += 1;

    debug_print(region, TEST_SIZE);

    if (d != NULL) {
        free_requests += 1;
        if (memory_free(d) == 0) free_correct += 1;
    }

    if (b != NULL) {
        free_requests += 1;
        if (memory_free(b) == 0) free_correct += 1;
    }

    if (c != NULL) {
        free_requests += 1;
        if (memory_free(c) == 0) free_correct += 1;
    }

    if (a != NULL) {
        free_requests += 1;
        if (memory_free(a) == 0) free_correct += 1;
    }

    debug_print(region, TEST_SIZE);

    printf("\t----------" ANSI_RED "|[RESULT]|" ANSI_RST "----------\n");

    debug_print_list(region);

    malloc_ratio = (malloc_correct / malloc_requests) * 100;
    free_ratio = (free_correct / free_requests) * 100;


    printf("\t\tMalloc Requests: %.0f\n", malloc_requests);
    printf("\t\tMalloc Correct: %.0f\n", malloc_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", malloc_ratio);

    printf("\n\t\tFree Requests: %.0f\n", free_requests);
    printf("\t\tFree Correct: %.0f\n", free_correct);
    printf("\t\tSuccess ratio: %.2f%%\n", free_ratio);

    return 0;
}

